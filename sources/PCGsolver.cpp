/*
 * PCGsolver.cpp
 *
 *  Created on: Nov 27, 2014
 *      Author: cgastald
 */


# include <cassert>
#include <math.h>
#include <cmath>

#include "PCGsolver.hpp"

/// Constructor

PCGsolver::PCGsolver(DenseMatrix& A, VectorND& b, VectorND& x)
: AbstractPreconditioner(A,b,x)
{}


PCGsolver::PCGsolver(PCGsolver& otherPCGsolver)
: AbstractPreconditioner(otherPCGsolver)
{}

// Destructor

PCGsolver::~PCGsolver()
{}


DenseMatrix PCGsolver::ComputeD(DenseMatrix& A)
{
	DenseMatrix D(A.size());

	for(int i=0;i<A.size();i++)
	{
		for(int j=0;j<A.size();j++)
		{
			if(j==i)
			{
				D(i,i)=A(i,i);
			}
			else
			{
				D(i,j)=0;
			}
		}

	}
	return D;
}

DenseMatrix PCGsolver::ComputeI(int size)
{
	DenseMatrix I(size);

	for(int i=0;i<size;i++)
	{
		for(int j=0;j<size;j++ )
		{
			if(j!=i)
			{
				I.SetElement(i,j,0);
			}
			else
			{
				I.SetElement(i,i,1);
			}
		}
	}
	return I;
}


VectorND PCGsolver::Solve()
{
	//Check of the Symmetry
	bool s=mpA.Symmetric();
	bool diag=mpA.Diag();
	if(s==false)
	{
		std::cerr<<"ERROR: Matrix A should be symmetric and positive definite. \n";
	}
	if (diag==false)
	{
		std::cerr<<"ERROR: Diagonal elements of matrix A must be bigger than  0. \n";
	}
	else
	{
		VectorND r(mSize);
		VectorND p(mSize);
		VectorND x(mSize);

		// initialize all the components of x to 1
		for (int i=0;i<mSize;i++)
		{
			x[i]=1;
		}

		//fix the initial condition on r
		r=mpb-mpA*mpx;


		char pc;

		std::cout<<"Do you want to precondition the system? (y/n) \n";
		std::cin>>pc;
		if (pc=='y')
		{
			mP=ComputeD(mpA);
		}
		else if (pc=='n')
		{
			mP=ComputeI(mSize);
		}
		else
		{
			std::cout<<"ERROR: choose if you want to precondition the system (y/n) \n";

		}


		//loop on iterations
		int t =0;
		int Tmax =1000;
		double alpha=0;
		double beta=0;

		while(fabs((mpx-x).maxComp()!=0. ))
		{
			x=mpx;

			LUsolver JA(mP,r,p);
			p=JA.Solve();

			//computation of alpha
			alpha=(p*r)/((mpA*p)*p);
			//computation of x
			mpx=mpx+p*alpha;
			//computation of r
			r=r-(mpA*p)*alpha;

			//end of iteration

			beta=((mpA*p)*r)/((mpA*p)*p);

			p=r-p*beta;
			t+=1;
			//Uncomment the following line to see the solution at each iteration
			//std::cout <<"mpx  at time "<<t<<" is "<<mpx<<"\n";
			if (t==Tmax)
			{
				std::cout <<"The maximum number of iterations "<<Tmax<< " has been reached without converging\n";
				break;
			}
		}
	}
	return mpx;
}





