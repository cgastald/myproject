/**
 * LinearSystem.hpp
 * Base abstract class for linear systems
 *
 *  Created on: Nov 17, 2014
 *      Author: Chiara Gastaldi
 */


///definition of the abstract class LinearSystem
#ifndef LINEARSYSTEM_HPP_
#define LINEARSYSTEM_HPP_

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <fstream>

#include "DenseMatrix.hpp"
#include "VectorND.hpp"

class LinearSystem
{
  //public methods
public:
	// Constructors
	/**
	 *Constructor : requires matrix and two vectors,
	 *the second vector doesn't need to be initialized.
	 *
	 * parameters: DenseMatrix& A, VectorND& b, VectorND& x
	 */
	LinearSystem(DenseMatrix& A, VectorND& b, VectorND& x);

	///Default constructor
	/**
	 * Empty constructor: initialize everything to 0
	 */
	LinearSystem();

	///copy constructor
	LinearSystem(LinearSystem& otherLinearSystem);

	/// Destructor
	virtual ~LinearSystem();




	// Access methods
	///Get method
	/**
	 *  Getb: returns the vector of the known terms
	 */
	VectorND Getb() const;

	///Get method
	/**
	 *  Getx: returns the solutions of the linear system
	 */
	VectorND Getx() const;

	///Get method
	/**
	 *  GetSize: returns the size of the linear system
	 */
	int GetSize() const;

	///Get method
	/**
	 *  GetA: returns the matrix of the coefficients
	 */
	DenseMatrix GetA()  const;

	///Set method
	/**
	 *  Setb: sets the vector of the known terms
	 *
	 *  parameters: VectorND b
	 */
	void Setb(VectorND b);


	///Set method
	/**
	 *  SetA: sets the matrix of the coefficients
	 *
	 *  parameters: DenseMatrix A
	 */
	void SetA(DenseMatrix A);


	/// Implementation of pure virtual method for solving the linear system
	virtual VectorND Solve()=0;

	///size of the linear system
	int mSize;

	///matrix for the linear system
	DenseMatrix mpA;

	///vector of known parameters of the linear system
	VectorND mpb;

	///vector of of solutions of the linear system
	VectorND mpx;


};

#endif /* LINEARSYSTEM_HPP_ */
