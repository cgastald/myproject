/**
 * AbstractMatrix.hpp
 *
 * Abstract base class for the matrix 
 *
 *  Created on: Nov 15, 2014
 *      Author: Chiara Gatstaldi
 *
 */

/// Definition of the abstract class AbstractMatrix

#ifndef ABSTRACTMATRIX_HPP_
#define ABSTRACTMATRIX_HPP_

#include <iostream>

#include "VectorND.hpp"

class AbstractMatrix {

	//public methods
public:
	/// Default constructor
	AbstractMatrix() {}

	/// Destructor
	virtual ~AbstractMatrix() {}

	/**
	 *  SetElement: set method used for insering elements into an empty matrix
	 *
	 *  Parameters:
	 *  	row, col - position in the matrix of the new element
	 *  	value    - values of the element to be inserted
	 */
	virtual void SetElement( int row,  int col, double value) = 0;
	/**
	 *  GetElement: get method used for reading elements from a matrix
	 *
	 *  Parameters:
	 *  	row, col - position in the matrix of the new element
	 */
	virtual double GetElement( int row,  int col) = 0;

	/// Gets the size of the matrix, assuming it is square
	/**
	 *  size: get method used for reading the size of the matrix
	 */
	virtual int size() const = 0;

	/// Operator *  
	/**
	 * operator* : allows performing w = M * v, with v,w of type VectorND
	 * and M of type AbstractMatrix
	 */
	virtual VectorND operator*(const VectorND& v) const = 0;

	/// Access operators for const values:
	/**
	 * operator() : allows reading the (i,j) element of the matrix
	 */
	virtual double operator() (int i, int j) const = 0;

	/// Public methods
	/**
	 * Print : allows to print the matrix to the command line
	 */
	virtual void Print(std::ostream& s = std::cout) = 0;
};

#endif /* ABSTRACTMATRIX_HPP_ */
