/*
 * LinearSistem.cpp
 *
 *  Created on: Nov 17, 2014
 *      Author: Chiara Gastaldi
 */

# include <cassert>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>

#include "LinearSystem.hpp"

// Constructor

LinearSystem::LinearSystem(DenseMatrix& A, VectorND& b, VectorND& x)
: mSize(A.size()),
  mpA(A.size()), //matrix for the linear system
  mpb(b), //vector for the linear system
  mpx(x)
{
	//check matrix and vector are of compatible size
	assert(b.GetN()==A.size());
	for(int i=0;i<A.size();i++)
	{
		for(int j=0;j<A.size();j++)
		{
			mpA.SetElement(i,j,A.GetElement(i,j));
		}
	}

}
//default constructor

LinearSystem::LinearSystem()
: mSize(3),
  mpA(3), //matrix for the linear system
  mpb(3), //vector for the linear system
  mpx(3)
{}

//Copy constructor

LinearSystem::LinearSystem(LinearSystem& otherLinearSystem)
: mSize(otherLinearSystem.GetSize()),
  mpA(otherLinearSystem.GetA()),
  mpb(otherLinearSystem.Getb()),
  mpx(otherLinearSystem.Getx())
{}


// Destructor

LinearSystem::~LinearSystem()
{}

/// Access methods

VectorND LinearSystem::Getb() const
{
	return mpb;
}

VectorND LinearSystem::Getx() const
{
	return mpx;
}

int LinearSystem::GetSize() const
{
	return mSize;
}

DenseMatrix LinearSystem::GetA()  const
{
	return mpA;
}


void LinearSystem::Setb(VectorND b)
{
	mpb=b;
}

void LinearSystem::SetA(DenseMatrix A)
{
	mpA=A;
}


