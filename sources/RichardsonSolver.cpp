/*
 * RichardsonSolver.cpp
 *
 *  Created on: Dec 9, 2014
 *      Author: cgastald
 */

# include <cassert>
#include <math.h>
#include <cmath>

#include "RichardsonSolver.hpp"

/// Constructor

RichardsonSolver::RichardsonSolver(DenseMatrix& A, VectorND& b, VectorND& x)
: AbstractPreconditioner(A,b,x)
{}

RichardsonSolver::RichardsonSolver(RichardsonSolver& RichardsonSolver)
: AbstractPreconditioner(RichardsonSolver)
{}
// Destructor

RichardsonSolver::~RichardsonSolver()
{}

DenseMatrix RichardsonSolver::ComputeD(DenseMatrix& A)
{
	DenseMatrix D(A.size());

	for(int i=0;i<A.size();i++)
	{

		for(int j=0;j<A.size();j++)
		{
			if(j==i)
			{
				D(i,i)=A(i,i);
			}
			else
			{
				D(i,j)=0;
			}
		}

	}
	return D;
}

DenseMatrix RichardsonSolver::ComputeI(int size)
{
	DenseMatrix I(size);

	for(int i=0;i<size;i++)
	{
		for(int j=0;j<size;j++ )
		{
			if(j!=i)
			{
				I.SetElement(i,j,0);
			}
			else
			{
				I.SetElement(i,i,1);
			}
		}
	}
	return I;
}



VectorND RichardsonSolver::Solve()
{
	bool s=mpA.Symmetric();
	bool diag=mpA.Diag();
	if(s==false)
	{
		std::cerr<<"ERROR: Matrix A should be symmetric. \n";
	}
	if (diag==false)
	{
		std::cerr<<"ERROR: Diagonal elements of matrix A must be bigger than  0. \n";
	}
	else
	{
		double alpha;
		std::cout<<"Please insert the value of alpha\n";
		std::cin>>alpha;

		VectorND r(mSize);
		VectorND z(mSize);
		VectorND x(mSize);

		// initialize all the components of x to 1
		for (int i=0;i<mSize;i++)
		{
			x[i]=1;
		}

		for(int i=0;i<mSize;i++)
		{
			double sum=0;
			for(int j;j<mSize;j++)
			{
				sum=sum+mpA.GetElement(i,j)*mpx[j];
			}
			r[i]=mpb[i]-sum;
		}


		char pc;

		std::cout<<"Do you want to precondition the system? (y/n) \n";
		std::cin>>pc;
		if (pc=='y')
		{
			mP=ComputeD(mpA);
		}
		else if (pc=='n')
		{
			mP=ComputeI(mSize);
		}
		else
		{
			std::cout<<"ERROR: choose if you want to precondition the system (y/n) \n";

		}

		int t =0;
		int Tmax =1000;


		while(fabs((mpx-x).maxComp())>0.001)
		{
			//Uncomment the following lines to see the result of each iteration
			/*
			std::cout<<"mpx at time "<< t<< " is "<<mpx<<"\n";*/
			x=mpx;

			LUsolver JA(mP,r,z);
			z=JA.Solve();

			//computation of x
			mpx=mpx+z*alpha;
			//computation of r
			r=r-(mpA*z)*alpha;

			t+=1;
			if (t==Tmax)
			{
				std::cout <<"The maximum number of iterations "<<Tmax<< " has been reached without converging\n";
				break;
			}
		}
	}

	return mpx;
}






