/*
 * GaussSaidel_test.cpp
 *
 *  Created on: Nov 26, 2014
 *      Author: cgastald
 */




#include <iostream>

#include "VectorND.hpp"
#include "AbstractMatrix.hpp"
#include "DenseMatrix.hpp"
#include "LinearSystem.hpp"
#include "GaussSeidelSolver.hpp"

int main(int argc, char* argv[])
{
	// Declare two matrices
	DenseMatrix C1(3);
	C1(0,0)= 1.;
	C1(0,1)= 0.;
	C1(0,2)= 1.;
	C1(1,0)= 0.;
	C1(1,1)= 2.;
	C1(1,2)= 0.;
	C1(2,0)= 1.;
	C1(2,1)= 0.;
	C1(2,2)= 3.;

	VectorND b(3);
	b[1]=2.;
	b[2]=4.;
	b[0]=1.;
	VectorND c(3);
	VectorND x(3);


	GaussSeidelSolver GS(C1,b,x);
	GS.Solve();
	std::cout << "the solution x is " <<	GS.Getx()  << std::endl;

	// the solution is x=(-0.5,1,1.5)

    return 0;
}







