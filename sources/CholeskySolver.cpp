/*
 * ChoesckySolver.cpp
 *
 *  Created on: Nov 24, 2014
 *      Author: cgastald
 */



# include <cassert>
#include <math.h>
#include <cmath>
#include <stdio.h>      /* printf */


#include "CholeskySolver.hpp"

// Constructor
CholeskySolver::CholeskySolver(DenseMatrix& A, VectorND& b, VectorND& x)
: LinearSystem(A,b,x)
{}

// Copy constructor

CholeskySolver::CholeskySolver(CholeskySolver& otherCholeskySolver)
: LinearSystem(otherCholeskySolver)
{}


// Destructor

CholeskySolver::~CholeskySolver()
{}


DenseMatrix CholeskySolver::ComputeR(DenseMatrix& A)
{
	bool s=mpA.Symmetric();
	bool diag=mpA.Diag();
	DenseMatrix R(mSize);
	if(s==false)
	{
		std::cerr<<"ERROR: Matrix A should be symmetric and positive definite. \n";
	}
	if (diag==false)
	{
		std::cerr<<"ERROR: Diagonal elements of matrix A must be bigger than 0. \n";
	}
	else
	{

		R.SetElement(0,0,sqrt(A.GetElement(0,0)));
		double sum=0;
		double square=0;

		for(int i=1;i<mSize;i++)
		{
			square=0;
			for(int j=0;j<=(i-1);j++)
			{
				sum=0;
				for(int k=0;k<=(j-1);k++)
				{
					sum= sum+R.GetElement(k,i)*R.GetElement(k,j);
				}
				R.SetElement(j,i,(1/R.GetElement(j,j))*(A.GetElement(i,j)-sum));
			}

			for(int k=0;k<=(i-1);k++)
			{
				square= square +(R.GetElement(k,i)*R.GetElement(k,i));
			}
			R.SetElement(i,i,sqrt(A.GetElement(i,i)-square));
		}
	}
	return R;
}


VectorND CholeskySolver::Solve()
{

	DenseMatrix R(mSize);
	R=ComputeR(mpA);
	VectorND y(mSize);
	y = (R.Transpose()).ForwardTriangularSolve(mpb);
	mpx = R.BackwardTriangularSolve(y);
	return mpx;
}

