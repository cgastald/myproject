/*+
 * PGsolver.hpp
 * Implementation of the preconditioned gradient method to solve linear systems
 *  Created on: Nov 27, 2014
 *      Author: cgastald
 */


/// definition of the class PGsolver
#ifndef PGSOLVER_HPP_
#define PGSOLVER_HPP_


#include <iostream>

#include "DenseMatrix.hpp"
#include "VectorND.hpp"
#include "LinearSystem.hpp"
#include "AbstractPreconditioner.hpp"

class PGsolver: public AbstractPreconditioner
{
  //public methods
public:
	/// Constructor
	/**
	 * The constructor takes a matrix
	 * and a vector and it is directly implemented from
	 * the constructor of the class "AbstractPreconditioner"
	 */
	PGsolver(DenseMatrix& A, VectorND& b, VectorND& x);

	///copy constructor
	PGsolver(PGsolver& PGsolver);


	/// Destructor
	~PGsolver();
	/**
	 * ComputeD: computes the diagonal of the input matrix
	 * parameters: DenseMatrix& A
	 */
	DenseMatrix ComputeD(DenseMatrix& A);

	/**
	 * ComputeI: computes the identity matrix
	 * parameters: int size, the size of the identity matrix
	 */
	DenseMatrix ComputeI(int size);

	///Solve method
	/**
	 * Solve(void): solves the linear system using preconditioned gradient method
	 */
	VectorND Solve();

private:





};


#endif /* PGSOLVER_HPP_ */
