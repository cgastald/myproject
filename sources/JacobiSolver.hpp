/**
 * JacobiSolver.hpp
 *Implementation of the Jacobi iterative method to solve linear systems
 *  Created on: Nov 26, 2014
 *      Author: Chiara Gastaldi
 */


///definition of the class JacobiSolver
#ifndef JACOBISOLVER_HPP_
#define JACOBISOLVER_HPP_

#include <iostream>

#include "DenseMatrix.hpp"
#include "VectorND.hpp"
#include "LinearSystem.hpp"

class JacobiSolver: public LinearSystem
{
  //public methods
public:
	/// Constructor
	/**
	 * The constructor takes a matrix
	 * and a vector and it is directly implemented from
	 * the constructor of the class "LinearSystem"
	 */
	JacobiSolver(DenseMatrix& A, VectorND& b, VectorND& x);

	///copy constructor
	JacobiSolver(JacobiSolver& otherJacobiSolver);


	/// Destructor
	~JacobiSolver();

	DenseMatrix ComputeD(DenseMatrix& A);


	///Solve method
	/**
	 * Solve(void): solves the linear system using Jacobi method
	 */
	VectorND Solve();

};


#endif /* JACOBISOLVER_HPP_ */
