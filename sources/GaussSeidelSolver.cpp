/*
 * GaussSeidel.cpp
 *
 *  Created on: Nov 26, 2014
 *      Author: cgastald
 */




# include <cassert>
#include <math.h>
#include <cmath>

#include "GaussSeidelSolver.hpp"

// Constructor

GaussSeidelSolver::GaussSeidelSolver(DenseMatrix& A, VectorND& b, VectorND& x)
: LinearSystem(A,b,x)
{}
// Copy constructor

GaussSeidelSolver::GaussSeidelSolver(GaussSeidelSolver& otherGaussSeidelSolver)
: LinearSystem(otherGaussSeidelSolver)
{}


// Destructor

GaussSeidelSolver::~GaussSeidelSolver()
{}


DenseMatrix GaussSeidelSolver::ComputeD(DenseMatrix& A)
{
	DenseMatrix D(A.size());
	for(int i=0;i<A.size();i++)
	{
		if(A(i,i)==0)
		{
			std::cerr<<"ERROR: diagonal elements must be bigger than  0\n";
		}
		else
		{
			for(int j=0;j<A.size();j++)
			{
				if(j==i)
				{
					D(i,i)=A(i,i);
				}
				else
				{
					D(i,j)=0;
				}
			}
		}
	}
	return D;
}


VectorND GaussSeidelSolver::Solve()
{
	bool diag=mpA.Diag();
	if (diag==false)
	{
		std::cerr<<"ERROR: Diagonal elements of matrix A must be different from 0. \n";
	}
	else
	{
		double sum1=0;
		double sum2=0;
		double sum=0;
		VectorND x(mSize);
		VectorND l(mSize);
		l[0]=1;

		int Tmax=1000;
		int t=0;

		while(fabs(l.maxComp())>0.001 )
		{
			//Unccomment the following lines to see the solution at each iteration
			/*std::cout <<"t is "<<t<<"\n";
			std::cout <<"x is "<<x<<"\n";*/
			x=mpx;

		///////////////////////////////////////
			//NOTE: the commented code is equivalent to the uncommented one
			/*for(int i=0;i<mSize;i++)
			{
				sum=0;
				for(int j=0;j<mSize;j++ )
				{
					if(j!=i)
					{
						sum=sum +(mpA.GetElement(i,j))*(mpx[j]);
					}

				}
				std::cout <<"sum is "<<sum<<"\n";
				mpx.SetElement(i,(1/mpA.GetElement(i,i))*(mpb.GetElement(i)-sum));
			}*/


			for(int i=0;i<mSize;i++)
			{
				sum1=0;
				sum2=0;
				for(int j=i+1;j<mSize;j++)
				{
						sum2=sum2+(mpA.GetElement(i,j))*(x[j]);
				}
				mpx.SetElement(i,(1/mpA.GetElement(i,i))*(mpb.GetElement(i)-sum2));

				for(int j=0;j<i;j++ )
				{

						sum1=sum1+(mpA.GetElement(i,j))*(mpx[j]);

				}
				mpx.SetElement(i,(1/mpA.GetElement(i,i))*(mpb.GetElement(i)-sum1-sum2));
			}


		///////////////////////////



			t+=1;
			if (t==Tmax)
			{
				std::cout <<"The maximum number of iterations "<<Tmax<< "has been reached without converging\n";
				break;
			}
			l=x-mpx;

		}
	}
	return mpx;
}








