/**
 * LUsolver.hpp
 *Implementation of the LU method to solve linear systems
 *
 *  Created on: Nov 17, 2014
 *      Author: Chiara Gastaldi
 */


///definition of the class LUsolver
#ifndef LUSOLVER_HPP_
#define LUSOLVER_HPP_

#include <iostream>

#include "DenseMatrix.hpp"
#include "VectorND.hpp"
#include "LinearSystem.hpp"

class LUsolver: public LinearSystem
{
  //public methods
public:
	/// Constructor
	/**
	 * The constructor takes a matrix
	 * and a vector and it is directly implemented from
	 * the constructor of the class "LinearSystem"
	 */
	LUsolver(DenseMatrix& A, VectorND& b, VectorND& x);

	///copy constructor
	LUsolver(LUsolver& LU);


	/// Destructor
	~LUsolver();

	/// Singularity check
	/**
	 * Singular: returns  bool true if the matrix in singular and false if it isn't
	 */
	bool Singular();


	DenseMatrix ComputeL(DenseMatrix& A, VectorND& b);

	void Pivot(VectorND& b, const int k);
	
	
	///Solve method
	/**
	 * Solve(void): solves the linear system using LU method
	 */
	VectorND Solve();

private:




};

#endif /* LUSOLVER_HPP_ */
