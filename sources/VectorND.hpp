/**
 * Vector3D.hpp
 * Implementation of the base class for vectors
 *  Created on: Nov 15, 2015
 *      Author: Chiara Gastaldi
 */


/// definition of the class VectorND
#ifndef VECTORND_HPP_
#define VECTORND_HPP_

#include<iostream>
#include <cassert>
class VectorND
{

// Public methods
public:
	/// Constructor
	/**
      * Initializes to 0 all the elements of the vector
      * Parameters: size of the vector
      */
	VectorND(int N);

	///Constructor
    /**
      * Parameters: size of the vector, empty array v[]
	  */
	VectorND(const int N,const double v[]);

	///Copy constructor
        /**
         *  Parameters: VectorND& v
         */
	VectorND(const VectorND& v);

    /// Destructor
	virtual ~VectorND();

	/// Get method
        /**  double* GetV(void)
         *  gives the vectorial part
         */
	double* GetV() const;

	/// Get method
        /**  int GetN(void) const
         *  gives the size of the vector
         */
	int GetN() const;

	/// Get method
        /**  double GetElement(int i)
         *  returns the i-th element
         */
	double GetElement(int i);

	/// Get method
        /**  double operator[](int i) const
         * returns the i-th element
         */
	double operator[] (int i) const;

	/// Set method
        /**  double operator[](int i)
         * sets the i-th element
         */
	double& operator[] (int i);

	   /// returns the absute value of max component of the vector
	double maxComp();


	/// Set method
        /**  void SetV(double v[])
         * sets the vectorial part
         */
	void SetV(double v[]);

	/// Set method
        /**  void SetN(const int N)
         * sets the size of the vector
         */
	void SetN(const int N);

	/// Set method
        /**  void SetElement(int i, double c)
         * sets the i-th element
         */
	void SetElement(int i, double c);

	/// Operations
	/// Assignment operator
	VectorND& operator=(const VectorND& v);
	/// Unary minus operator
	VectorND operator-() const;
	/// Binary addition operator
	VectorND operator+(const VectorND& v) const;
	/// Binary substraction operator
	VectorND operator-(const VectorND& v) const;
	/// Binary vector * double operator
	VectorND operator*(const double d) const;
	/// Binary vector * vector operator
	double operator*(const VectorND d) const;
	/// Dot product of two vectors
	double dot(const VectorND& v) const;

	// Other public methods
	void Print(std::ostream& s = std::cout);

	///Load Vector
	/**
	 *  Load_vector: import a vector from a .txt file (Max size = 1000)
	 *
	 */
	void Load_vector();

	// Friend functions
	friend std::ostream& operator<<(std::ostream& output, const VectorND& v);

private:
	// Data members
	int mN;
	double* mV;

};

#endif /* VECTORND_HPP_ */
