/*
 * JacobiSolver.cpp
 *
 *  Created on: Nov 26, 2014
 *      Author: cgastald
 */


# include <cassert>
#include <math.h>
#include <cmath>
#include <iostream>

#include "JacobiSolver.hpp"

/// Constructor

JacobiSolver::JacobiSolver(DenseMatrix& A, VectorND& b, VectorND& x)
: LinearSystem(A,b,x)
{}
/// Copy constructor

JacobiSolver::JacobiSolver(JacobiSolver& otherJacobiSolver)
: LinearSystem(otherJacobiSolver)
{}


/// Destructor

JacobiSolver::~JacobiSolver()
{}


DenseMatrix JacobiSolver::ComputeD(DenseMatrix& A)
{
	DenseMatrix D(A.size());

	for(int i=0;i<A.size();i++)
	{
		if(A(i,i)==0)
		{
			std::cerr<<"ERROR: diagonal elements must be bigger than  0\n";
		}
		else
		{
			for(int j=0;j<A.size();j++)
			{
				if(j==i)
				{
					D(i,i)=A(i,i);
				}
				else
				{
					D(i,j)=0;
				}
			}
		}
	}
	return D;
}


VectorND JacobiSolver::Solve()
{
	bool diag=mpA.Diag();
	if (diag==false)
	{
		std::cerr<<"ERROR: Diagonal elements of matrix A must be different from 0. \n";
	}
	else
	{
		double sum=0;
		VectorND x(mSize);
		VectorND l(mSize);
		l[0]=1;

		int Tmax=1000;
		int t=0;

		while(fabs(l.maxComp())>0.001 )
		{
			x=mpx;
			for(int i=0;i<mSize;i++)
			{
				sum=0;
				for(int j=0;j<mSize;j++ )
				{
					if(j!=i)
					{
						sum=sum +(mpA.GetElement(i,j))*(x[j]);
					}

				}
				mpx.SetElement(i,(1/mpA.GetElement(i,i))*(mpb.GetElement(i)-sum));
			}
			t+=1;
			if (t==Tmax)
			{
				std::cout <<"The maximum number of iterations "<<Tmax<< " has been reached without converging\n";
				break;
			}
			l=x-mpx;
			//NOTE: Uncomment the following line to see the solution at each iteration
			//std::cout <<"mpx  at time "<<t<<" is "<<mpx<<"\n";
		}
	}
	return mpx;
}









