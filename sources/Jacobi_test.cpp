/*
 * Jacobi_test.cpp
 *
 *  Created on: Nov 26, 2014
 *      Author: cgastald
 */
#include <iostream>

#include "VectorND.hpp"
#include "AbstractMatrix.hpp"
#include "DenseMatrix.hpp"
#include "LinearSystem.hpp"
#include "JacobiSolver.hpp"

int main(int argc, char* argv[])
{
	//---FIRST TEST---
	// Declare two matrices
	DenseMatrix C1(3);
	C1(0,0)= 1.;
	C1(0,1)= 0.;
	C1(0,2)= 1.;
	C1(1,0)= 0.;
	C1(1,1)= 2.;
	C1(1,2)= 0.;
	C1(2,0)= 1.;
	C1(2,1)= 0.;
	C1(2,2)= 3.;

	DenseMatrix C2(3);
	C2(0,0)= 1.;
	C2(0,1)= 4.;
	C2(0,2)= 2.;
	C2(1,0)= 3.;
	C2(1,1)= 6.;
	C2(1,2)= 2.;
	C2(2,0)= 1.;
	C2(2,1)= 3.;
	C2(2,2)= 1.;

	VectorND b(3);
	b[0]=1.;
	b[1]=2.;
	b[2]=4.;

	VectorND c(3);
	VectorND x(3);
	DenseMatrix C3(3);

	JacobiSolver JA(C1,b,x);
	C3=JA.ComputeD(C1);
	std::cout << "D is " <<	C3  << std::endl;
	x=JA.Solve();
	std::cout << "the solution x is " <<	x  << std::endl;

	// the solution with C1 is x=(-0.5,1,1.5)
	// the solution with C2 is x=(-6,6.5,-9.5) but with this matrix the Jacobi method doesn't converge

	//---SECOND TEST---

	DenseMatrix r(3);
	r(0,0)= 1.;
	r(0,1)= -4.;
	r(0,2)= 0.;
	r(1,0)= -4.;
	r(1,1)= -1.;
	r(1,2)= 0.;
	r(2,0)= 0.;
	r(2,1)= 0.;
	r(2,2)= 8.;

	VectorND f(3);
	f[1]=1.;
	f[2]=1.;
	f[0]=1.;
	VectorND d(3);
	VectorND y(3);
	DenseMatrix C4(3);

	JacobiSolver CH(r,f,y);
	// example of wrong choice of the matrix-> convergence is not reached
	y = CH.Solve();
	std::cout << "the solution y is " <<	y  << std::endl;

	return 0;
}







