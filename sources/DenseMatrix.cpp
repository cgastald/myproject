/*
 * DenseMatrix.cpp
 *
 * Concrete derived class of AbstractMatrix implementing a dense storage format
 *
 *  Created on: Nov 11, 2014
 *      Author: Chiara Gastaldi
 */

#include <cassert>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <stdio.h>      /* printf */
#include <stdlib.h>     /* abs */
#include <sstream>
#include <string>




#include "DenseMatrix.hpp"

// Constructors

DenseMatrix::DenseMatrix(const int size)
{
	mSize = size;
	mMatrix  = new double* [mSize];
    for (int i=0; i<mSize; i++)
    {
    	mMatrix [i] = new double [mSize];
    }
    for (int i=0; i<mSize; i++)
    {
        for (int j=0; j<mSize; j++)
        {
        	mMatrix [i][j] = 0.0;
        }
    }
}


// Copy Constructor
DenseMatrix::DenseMatrix(const DenseMatrix& M)
{
		mSize = M.mSize;
		mMatrix = new double* [mSize];
	    for (int i=0; i<mSize; i++)
	    {
	    	mMatrix[i] = new double [mSize];
	    }
	    for (int i=0; i<mSize; i++)
	    {
	        for (int j=0; j<mSize; j++)
	        {
	        	mMatrix[i][j] = M.mMatrix[i][j];
	        }
	    }
}


// Destructor

DenseMatrix::~DenseMatrix()
{

	for (int i = 0; i < mSize; ++i)
	{
		delete[] mMatrix[i];
	}
	delete[] mMatrix;
}



// Set method

void DenseMatrix::SetElement( int row,
							  int col,
							 double value)
{
	mMatrix[row][col] = value;
}

double DenseMatrix::GetElement( int row,
							    int col)
{
	return mMatrix[row][col];
}


void DenseMatrix::Load_matrix()
{

  int x, y;
  std::ifstream in("matrix.txt");

  if (!in) {
    std::cout << "Cannot open file.\n";
    return;
  }

  for (y = 0; y < mSize; y++)
  {
    for (x = 0; x < mSize; x++)
    {
      in >> mMatrix[x][y];
    }
  }

  in.close();
}


// Operators

VectorND DenseMatrix::operator *(const VectorND& v) const
{
	VectorND w(mSize);

	for (int i = 0; i < mSize; ++i)
	{
		for (int j = 0; j < mSize; ++j)
		{
			w[i] += mMatrix[i][j] * v[j];
		}
	}

	return w;
}

DenseMatrix DenseMatrix::operator *(const double v) const
{
	DenseMatrix w(mSize);

	for (int i = 0; i < mSize; ++i)
	{
		for (int j = 0; j < mSize; ++j)
		{
			w(i,j) = mMatrix[i][j] * v;
		}
	}

	return w;
}



// Other public methods

int DenseMatrix::size() const
{
	return mSize;
}

double ** DenseMatrix::GetM()const
{
	return mMatrix;
}

VectorND DenseMatrix::ForwardTriangularSolve(VectorND& b) const
{
	VectorND y(mSize);
	double sum=0.;
	y[0] = (1/mMatrix[0][0])*b[0];
		for (int i = 1; i < mSize; i++)
		{
			sum=0.;

			for (int j = 0; j <= i-1; j++)
			{

				sum = sum + mMatrix[i][j] * y[j];

			}
			y[i] = (1/mMatrix[i][i])*( b[i]-sum);
		}
		return y;
}

VectorND DenseMatrix::BackwardTriangularSolve( VectorND& b) const
{
	VectorND x(mSize);
	double sum=0.;
	x[mSize-1]=(1/mMatrix[mSize-1][mSize-1])*b[mSize-1];
	for (int i = mSize - 2; i >= 0; i--)
	{

		sum=0.;
		for (int j = i+1 ; j < mSize; ++j)
		{
			sum = sum + mMatrix[i][j] * x[j];

		}

		x[i] = (1/mMatrix[i][i])*( b[i]-sum);
	}
	return x;
}



bool DenseMatrix::Symmetric() const
{
	bool symmetry= true;
	for (int i=0;i<mSize;i++)
	{
		for(int j=0; j<mSize; j++)
		{
			if (mMatrix[i][j]!=mMatrix[j][i])
			{
				symmetry=false;
			}
		}
	}
	return symmetry;
}

bool DenseMatrix::Diag() const
{
	bool diag= true;
	for (int i=0;i<mSize;i++)
	{
		if (mMatrix[i][i]<=0.)
		{
			diag=false;
		}
	}
	return diag;
}


double DenseMatrix::Trace() const
{
	double trace=0;
	for (int i=0;i<mSize;i++)
	{
		trace =trace + mMatrix[i][i];
	}
	return trace;
}


DenseMatrix DenseMatrix::Transpose()
{
	DenseMatrix T(mSize);
	for (int i=0;i<mSize;i++)
	{
		for(int j=0; j<mSize; j++)
		{
			T(i,j)=mMatrix[j][i];
		}
	}
	return T;
}


// Access operators for const values:
double DenseMatrix::operator() (int i, int j) const
{
	// First we should check that the entries are within range
	assert( i < mSize && j < mSize );

	return mMatrix[i][j];

}

// Access operators for non-const values:
double& DenseMatrix::operator() (int i, int j)
{
	// First we should check that the entries are within range
	assert( i < mSize && j < mSize );

	return mMatrix[i][j];
}


DenseMatrix& DenseMatrix::operator=(DenseMatrix const& A)
{
	 assert(mSize = A.size());
        for (int i=0; i<mSize; i++)
        {
            for (int j=0; j<mSize; j++)
            {
                mMatrix[i][j] = A.mMatrix[i][j];
            }
        }
        return *this;
}


DenseMatrix DenseMatrix::operator+(DenseMatrix const& A)
{

	DenseMatrix B(A.size());
	for (int i = 0; i < A.size(); ++i)
	{
		for (int j = 0; j < A.size(); ++j)
		{
			B(i,j)=A(i,j)+mMatrix[i][j];
		}
	}

    return B;
}

DenseMatrix DenseMatrix::operator-(DenseMatrix const& A)
{

	DenseMatrix B(A.size());

	for (int i = 0; i < A.size(); ++i)
	{
		for (int j = 0; j < A.size(); ++j)
		{

			B.mMatrix[i][j]=mMatrix[i][j]-A.mMatrix[i][j];
		}
	}

	return B;
}

void DenseMatrix::Print(std::ostream& s)
{
	std::cout << "\n";
	for (int i = 0; i < mSize; ++i)
	{
		for (int j = 0; j < mSize; ++j)
		{
			s << mMatrix[i][j] << " ";
		}
		s << std::endl;
	}
	std::cout << "\n";
}

// Friend functions
std::ostream& operator<<(std::ostream& output, const DenseMatrix& v)
{
	output << "\n";
	for (int i=0;i<v.size();i++)
	 {
		 for(int j=0;j<v.size();j++)
		 {
			 output  <<  v.GetM()[i][j]<<" ";
		 }
		 output << "\n";
	 }
	 output << "\n";

	return output;
}

