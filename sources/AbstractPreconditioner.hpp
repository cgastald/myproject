/**
 * AbstractPreconditioner.hpp
 *
 * Abstract class for preconditioned systems
 *
 *  Created on: Nov 27, 2014
 *      Author: Chiara Gastaldi
 */


/// definition of the abstract class AbstractPreconditioner
#ifndef ABSTRACTPRECONDITIONER_HPP_
#define ABSTRACTPRECONDITIONER_HPP_

#include <iostream>

#include "VectorND.hpp"
#include "DenseMatrix.hpp"
#include "LinearSystem.hpp"
#include "LUsolver.hpp"
#include "JacobiSolver.hpp"

class AbstractPreconditioner: public LinearSystem
{

  ///public methods
public:
	/// Constructors
	AbstractPreconditioner(DenseMatrix& A, VectorND& b, VectorND& x);

	///copy constructor
	AbstractPreconditioner(AbstractPreconditioner& AB);


	/// Destructor
	virtual ~AbstractPreconditioner();

	//Access methods

	/**
	 *
	 *  SetP : sets the preconditioner matrix
	 *  Parameters:
	 *  	DenseMatrix& A   preconditioner
	 */
	void SetP(DenseMatrix& A);
	/**
	 *  SetAlpha: sets the parameter alpha
	 *
	 *  parameters: double alpha
	 */
	void SetAlpha(double alpha);

	//Get method
	/**
	 *  Getb: returns the parameter alpha
	 */
	double GetAlpha()const;
	/**
	 *  GetP: returns the preconditioner matrix
	 */
	DenseMatrix GetP();


	///Solve method
	/**
	 *
	 *  Solve: pure virtual method
	 */
	virtual VectorND Solve()=0;

	///Public member
	/**
	 *  mP: preconditioner matrix
	 *  mpAlpha: alpha parameter
	 */
	DenseMatrix mP;
	double mpAlpha;
};


#endif /* ABSTRACTPRECONDITIONER_HPP_ */
