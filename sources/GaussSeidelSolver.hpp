/**
 * GaussSeidel.hpp
 *Implementation of the Gauss and Seidel method to solve linear systems
 *  Created on: Nov 26, 2014
 *      Author: Chiara Gastaldi
 */
///Definition of the class GaussSeidelSolver
#ifndef GAUSSSEIDELSOLVER_HPP_
#define GAUSSSEIDELSOLVER_HPP_
#include <iostream>

#include "DenseMatrix.hpp"
#include "VectorND.hpp"
#include "LinearSystem.hpp"


class GaussSeidelSolver: public LinearSystem
{
  ///public methods
public:
	/// Constructor
	/**
	 * The constructor takes a matrix
	 * and a vector and it is directly implemented from
	 * the constructor of the class "LinearSystem"
	 */
	GaussSeidelSolver(DenseMatrix& A, VectorND& b, VectorND& x);

	///copy constructor
	GaussSeidelSolver(GaussSeidelSolver& otherGaussSeidelSolver);


	/// Destructor
	~GaussSeidelSolver();

	DenseMatrix ComputeD(DenseMatrix& A);

	///Solve method
	/**
	 * Solve(void): solves the linear system using GaussSeidel method
	 */
	VectorND Solve();

};




#endif /* GAUSSSEIDELSOLVER_HPP_ */
