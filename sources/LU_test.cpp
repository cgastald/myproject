#include <iostream>

#include "VectorND.hpp"
#include "AbstractMatrix.hpp"
#include "DenseMatrix.hpp"
#include "LinearSystem.hpp"
#include "LUsolver.hpp"

int main(int argc, char* argv[])
{
	DenseMatrix C1(3);
	C1(0,0)= 1.;
	C1(0,1)= 4.;
	C1(0,2)= 2.;
	C1(1,0)= 3.;
	C1(1,1)= 6.;
	C1(1,2)= 2.;
	C1(2,0)= 1.;
	C1(2,1)= 3.;
	C1(2,2)= 1.;
	
	VectorND b(3);
	b[1]=2.;
	b[2]=4.;
	b[0]=1.;
	VectorND c(3);
	VectorND x(3);
	
    DenseMatrix L(3);
    DenseMatrix A(3);
	LUsolver LU(C1,b,x);

	x= LU.Solve();

	std::cout << "the solution x is " <<	x  << std::endl;
	//the solution is x=(-6,6.5,-9.5)
	DenseMatrix C4(3);
	C4(0,0)= 1.;
	C4(0,1)= 0.;
	C4(0,2)= 1.;
	C4(1,0)= 0.;
	C4(1,1)= 2.;
	C4(1,2)= 0.;
	C4(2,0)= 1.;
	C4(2,1)= 0.;
	C4(2,2)= 3.;
	LUsolver LS(C4,b,x);
	x= LS.Solve();

	std::cout << "the solution x is " <<	x  << std::endl;
	//the solution is x=(-0.5,1,1.5)

	DenseMatrix D1(5);
	VectorND V1(5);
	VectorND x1(5);

	D1.Load_matrix();
	V1.Load_vector();
	std::cout<<D1<<std::endl;
	std::cout<<V1<<std::endl;
	LUsolver L1(D1,V1,x1);

	x1= L1.Solve();
    bool sin=LS.Singular();
	std::cout << "the solution x is " <<	x1  << std::endl;
	std::cout << "Is the matrix singular? 0 for no and 1 for yes\n " <<	sin  << std::endl;



    return 0;
}



