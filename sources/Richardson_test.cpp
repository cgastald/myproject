/*
 * Richardson_test.cpp
 *
 *  Created on: Dec 9, 2014
 *      Author: cgastald
 */




#include <iostream>

#include "VectorND.hpp"
#include "AbstractMatrix.hpp"
#include "DenseMatrix.hpp"
#include "LinearSystem.hpp"
#include "AbstractPreconditioner.hpp"
#include "RichardsonSolver.hpp"

int main(int argc, char* argv[])
{
	// Declare two matrices
	DenseMatrix C1(3);
	C1(0,0)= 1.;
	C1(0,1)= 0.;
	C1(0,2)= 1.;
	C1(1,0)= 0.;
	C1(1,1)= 2.;
	C1(1,2)= 0.;
	C1(2,0)= 1.;
	C1(2,1)= 0.;
	C1(2,2)= 3.;
	DenseMatrix C2(3);
	C2(0,0)= 1.;
	C2(0,1)= 4.;
	C2(0,2)= 2.;
	C2(1,0)= 3.;
	C2(1,1)= 6.;
	C2(1,2)= 2.;
	C2(2,0)= 1.;
	C2(2,1)= 3.;
	C2(2,2)= 1.;

	VectorND b(3);
	b[1]=2.;
	b[2]=4.;
	b[0]=1.;
	VectorND c(3);
	VectorND x(3);


	RichardsonSolver PG(C1,b,x);
	x=PG.Solve();
	std::cout << "the solution x is " <<	PG.Getx()  << std::endl;
	//the solution from C1 is x=(-0.5,1,1.5)



    return 0;
}

