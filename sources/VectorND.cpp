/*
 * VectorND.cpp
 *
 *  Created on: Oct 18, 2012
 *      Author: rpopescu
 */

#include "VectorND.hpp"
#include <stdio.h>      /* printf */
#include <stdlib.h>     /* abs */
#include <math.h>
#include <sstream>
#include <string>
#include <fstream>


// Constructors

VectorND::VectorND(int N)
	: mN(N),
	  mV(new double [N])
{

	for (int i=0;i<N;i++) mV[i]=0.0;
}

VectorND::VectorND(const int N,const double v[])
	: mN(N),
	  mV(new double [N])
{
	for (int i=0;i<N;i++) mV[i]=v[i];
}


// Copy constructor
VectorND::VectorND(const VectorND& v)
	//: mN(v.GetN()),
	 // mV(v.GetV())
{
	mN = v.GetN();
	    mV = new double [mN];
	    for (int i=0; i<mN; i++)
	    {
	    	mV[i] = v.mV[i];
	    }
}

// Destructor

VectorND::~VectorND()
{

	delete[] mV;
}

// Accessor methods

double* VectorND::GetV() const
{
	return mV;
}

int VectorND::GetN() const
{
	return mN;
}

double VectorND::GetElement(int i)
{
	return mV[i];
}

// Access operators for const values:
double VectorND::operator[] (int i) const
{
	// First we should check that the entries are within range
	assert( i < mN);

	return mV[i];

}

// Access operators for non-const values:
double& VectorND::operator[] (int i)
{
	// First we should check that the entries are within range
	assert( i < mN  );

	return mV[i];

}

double VectorND::maxComp()
{
	double a=0;
	for (int i=0;i<mN;i++)
	{
		if (fabs(mV[i])>a)
		{
			a=fabs(mV[i]);
		}
	}
	return a;
}
// Set methods

void VectorND::SetV(double v[])
{
	mV = v;
}

void VectorND::SetN(const int N)
{
	mN = N;
}

void VectorND::SetElement(int i, double c)
{
	mV[i] = c;
}

// Operations


VectorND& VectorND::operator=(const VectorND& v)
{
    assert(mN == v.mN);
    for (int i=0; i<mN; i++)
    {
        mV[i] = v.mV[i];
    }
    return *this;
}

VectorND VectorND::operator-() const
{
	VectorND v(mN);
	for (int i=0;i<mN;i++) mV[i]=- mV[i];

	return v;
}

VectorND VectorND::operator+(const VectorND& v) const
{
	assert (mN==v.GetN());
	VectorND w(mN);
	for (int i=0;i<mN;i++)
	{
		double c=(mV[i] + v.GetV()[i]);
		w.SetElement(i,c);
	}
	return w;
}

VectorND VectorND::operator-(const VectorND& v) const
{
	assert (mN==v.GetN());
	VectorND w(mN);
	for (int i=0;i<mN;i++)
		{
			double c =(mV[i] - v.GetV()[i]);
			w.SetElement(i,c);
		}

	return w;
}

VectorND VectorND::operator*(const double d) const {
	VectorND v(mN);
	for (int i=0;i<mN;i++)
		{
			double c=(d * mV[i]);
			v.SetElement(i,c);
		}

	return v;
}

double VectorND::operator*(const VectorND d) const
{
	double sum=0;
	for(int i=0; i<mN; i++)
	{
		sum=sum+d[i]*mV[i];
	}
	return sum;
}

double VectorND::dot(const VectorND& v) const {
	double d=0.0;
	for (int i=0;i<mN;i++) d += mV[i] * v.GetV()[i];
	return d;
}

// Other public methods

void VectorND::Print(std::ostream& s)
{
	s <<"(";
	for (int i=0;i<mN;i++)
		{
			s <<  mV[i]<<" ";
		}
	s<<")\n";
}

void VectorND::Load_vector()
{

	int x;
	std::ifstream read_file("vector.txt");

	if (!read_file)
	{
	std::cout << "Cannot open file.\n";
	return;
	}

	for (x = 0; x < mN; x++)
	{
		read_file >> mV[x];
	}


	read_file.close();
}

// Friend functions

std::ostream& operator<<(std::ostream& output, const VectorND& v) //funzione, che non e membro della classe
{
	 output << "(";
	 for (int i=0;i<v.GetN();i++)
		 {
		 	 output  <<  v.GetV()[i]<<" ";
		 }
	 output << ")\n";

	return output;
}
