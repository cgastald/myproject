/**
 * CholeskySolver.hpp
 *
 * Implementation of the Cholesky method to solve linear systems
 *
 *  Created on: Nov 24, 2014
 *      Author: Chiara Gastaldi
 */


/// definition of the class CholeskySolver
#ifndef CHOLESKYSOLVER_HPP_
#define CHOLESKYSOLVER_HPP_

#include <iostream>

#include "DenseMatrix.hpp"
#include "VectorND.hpp"
#include "LinearSystem.hpp"


class CholeskySolver: public LinearSystem
{
  //public methods
public:
	/// Constructor
	/** 
	 *Constructor : requires matrix and two vectors, 
	 *the second vector doesn't need to be initialized.
	 *It is directly implemented from the LinearSystem constructor
	 *
	 * parameters: DenseMatrix& A, VectorND& b, VectorND& x
	 */
	CholeskySolver(DenseMatrix& A, VectorND& b, VectorND& x);

	/// Copy constructor
	/**
	 *Copy Constructor : directly implemented from the LinearSystem copy constructor
	 */
	CholeskySolver(CholeskySolver& otherCholeskySolver);


	/// Destructor
	~CholeskySolver();


	/**
	 * ComputeR : reads a symmetric and positive definite matrix A and return the matrix R
	 * parameters: DenseMatrix& A
	 */
	DenseMatrix ComputeR(DenseMatrix& A);

	///Solve method
	/**
	 * To solve a linear system, Ax=b, with Cholesky method, the matrix A is required to be symmetric and positive definite.
 	 * Then A=R.Transpose*R
	 *
	 * Solve(void): solves the linear system using Cholesky method
	 */
	VectorND Solve();

private:




};


#endif /* CHOLESKYSOLVER_HPP_ */
