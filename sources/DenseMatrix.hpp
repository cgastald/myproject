/**
 * DenseMatrix.hpp
 *
 * Concrete derived class of AbstractMatrix implementing a dense storage format
 *
 *  Created on: Nov 15, 2014
 *      Author: Chiara Gastaldi
 */


///Definition of the class DenseMatrix (only for implementation of square matrices)
#ifndef DENSEMATRIX_HPP_
#define DENSEMATRIX_HPP_

#include <iostream>
#include <cassert>
#include <sstream>
#include <string>
#include <fstream>

#include "AbstractMatrix.hpp"
#include "VectorND.hpp"

class DenseMatrix : public AbstractMatrix {
  //public methods
public:
	/// Constructor
	/**
	 *  Parameters:
	 *  	size  dimension of the square matrix
	 */
	DenseMatrix(const int size);

	///Copy constructor
	DenseMatrix(const DenseMatrix& M);

	/// Destructor
	~DenseMatrix();

	/// Set method
	/**
	 *  Implementation of pure virtual method
	 *  SetElement: set method used for inserting elements into an empty matrix
	 *
	 *  Parameters:
	 *  	row, col - position in the matrix of the new element
	 *  	value    - values of the element to be inserted
	 */
	void SetElement( int row, int col, double value);

	///Get method
	/**
	 *  Implementation of pure virtual method
	 *
	 *  size: returns the size of the matrix
	 *
	 */
	int size() const;

	///Get method
	/**
	 *  Implementation of pure virtual method
	 *  GetM: returns a double pointer
	 *
	 */
	double ** GetM()const;

 
	/**
	 * ForwardTriangularSolve:  finds x from Ax=b where A is lower triangular
	 */
	VectorND ForwardTriangularSolve(VectorND& b) const;
	/**
	 * BackwardTriangularSolve:  finds x from Ax=b where A is upper triangular
	 */
	VectorND BackwardTriangularSolve(VectorND& b) const;


	/**
	 * Symmetry: returns bool true if the matrix is symmetric, or false if it isn't
	 */
	bool Symmetric() const;

	/**
	 * Diag: returns bool true if the diagonal values of matrix are positive,
	 *  or false if they aren't
	 */
	bool Diag() const;

	/**
	 * Trace: returns the sum of the diagonal values of the matrix
	 */
	double Trace() const;

	/**
	 * Transpose: returns the transposed matrix
	 */
	DenseMatrix Transpose();


	// Operators
	/// Operator *  
	/**
	 * operator* : allows performing w = M * v, where v,w of type VectorND
	 * and M of type AbstractMatrix and B=A*c, where B and A are matrices and c is a constant
	 */
	VectorND operator*(const VectorND& v) const;
	DenseMatrix operator*(const double v) const;


	// Access operators for const values:
	/// Access operators for const values:
	/**
	 * operator() : allows reading the (i,j) element of the matrix
	 */
	double operator() (int i, int j) const;

	/// Access operators for non-const values:
	/**
	 * operator() : allows setting the (i,j) element of the matrix
	 */
	double& operator() (int i, int j);
	
	///  Operator =
	/**
	 * operator = : allows to set a matrix equal to another one
	 */
	DenseMatrix& operator=(DenseMatrix const& A);

	///  Operator +
	/**
	 * operator + : allows to sum two matrices element by element
	 */
	DenseMatrix operator+(DenseMatrix const& A);
	///  Operator -
	/**
	 * operator - : allows to substruct two matrices element by element
	 */
	DenseMatrix operator-(DenseMatrix const& A);

	///Get method
	/**
	 *  Implementation of pure virtual method
	 *  GetElement: returns the element in the position (row, col)
	 *
	 *  parameters: int row, int col
	 *
	 */
	double GetElement( int row, int col);

	///Load matrix
	/**
	 *  Load_matrix: imports a matrix from a .txt file (Max size = 1000)
	 *
	 */
	void Load_matrix();


	/// Print
	/**
	 *  Print: prints the matrix to the command line
	 *
	 */
	void Print(std::ostream& s = std::cout);
	// Friend functions
	///operator <<
	/**
	 *  operator <<: prints the matrix to the command line using std::cout
	 *
	 */
	friend std::ostream& operator<<(std::ostream& output, const DenseMatrix& v);

	// Private data
private:
	/**
	 *  mSize: size of the matrix
	 *  mMatrix: double pointer 
	 */
	int mSize;
	double** mMatrix;
};

#endif /* DENSEMATRIX_HPP_ */
