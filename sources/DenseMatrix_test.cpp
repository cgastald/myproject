# include <cassert>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <stdio.h>      /* printf */
#include <stdlib.h>
#include <sstream>
#include <string>



#include "VectorND.hpp"
#include "AbstractMatrix.hpp"
#include "DenseMatrix.hpp"

int main(int argc, char* argv[])
{
	DenseMatrix C1(3);
	C1(0,0)= 1.;
	C1(0,1)= 0.;
	C1(0,2)= 1.;
	C1(1,0)= 0.;
	C1(1,1)= 2.;
	C1(1,2)= 0.;
	C1(2,0)= 1.;
	C1(2,1)= 0.;
	C1(2,2)= 3.;

	DenseMatrix C2(3);
	C2(0,0)= 0.;
	C2(0,1)= 5.;
	C2(0,2)= 3.;
	C2(1,0)= 0.;
	C2(1,1)= 1.;
	C2(1,2)= 0.;
	C2(2,0)= 1.;
	C2(2,1)= 2.;
	C2(2,2)= 0.;

	DenseMatrix C3(3);
	C3(0,0)= 1.;
	C3(0,1)= 0.;
	C3(0,2)= 0.;
	C3(1,0)= 3.;
	C3(1,1)= 1.;
	C3(1,2)= 0.;
	C3(2,0)= 1.;
	C3(2,1)= 0.5;
	C3(2,2)= 1.;

	DenseMatrix Z(3);
	Z(0,0)= 2.;
	Z(0,1)= 1.;
	Z(0,2)= 0.;
	Z(1,0)= 0.;
	Z(1,1)= 3.;
	Z(1,2)= 2.;
	Z(2,0)= 0.;
	Z(2,1)= 0.;
	Z(2,2)= 1.;

	double h[3]={1.,2.,4.};
	VectorND b(3,h);
	VectorND w(3);

	std::cout <<"vector b is "<< b<<"\n" ;

	std::cout <<"C3 forward solved"<< C3.ForwardTriangularSolve(b)<<"\n" ;
	// solution (1 -1 3.5)
	w=Z.BackwardTriangularSolve(b);
	std::cout <<"Z backwardsolved"<< w<<"\n" ;
	// solution (1.5 -2 4)

	C2.SetElement(1,1,4);
	C2.Print();

	std::cout << "\n" ;
	std::cout << C2(0,1)<<"\n" ;
	std::cout << "trace of C2 is"<<C2.Trace()<<"\n" ;
	std::cout <<"is C2 symmetric? "<<C2.Symmetric()<<"\n" ;
	// D= C1 + C2
	DenseMatrix D(3);
	D=C1;
	std::cout << "D = C1 : " << D << std::endl;
	D=C1 + C2;
	D.Print();
	std::cout << "D = C1 + C2: " << D << std::endl;

	D = C1 - C2;
	std::cout << "D = C1 - C2: " <<	D << std::endl;

	// c = z * C2
	double z=5.0;
	D =  C2*z;
	std::cout << "D = C2*5: " << D << std::endl;

	// d = C2 * c
	VectorND d(3);
	double a[3]={1.,1.,1.};
	VectorND c(3,a);
	d= (C2*c);
	std::cout << "d = C2 * c: " << d << std::endl;

	std::cout << "C2 transpose is : " << C2.Transpose() << std::endl;


    // test of load_matrix function
	DenseMatrix D1(5);
	VectorND V1(5);

	D1.Load_matrix();
	V1.Load_vector();
	std::cout<<D1<<std::endl;
	std::cout<<V1<<std::endl;



    return 0;
}



