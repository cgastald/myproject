/*
 * LinearSistem.cpp
 *
 *  Created on: Nov 17, 2014
 *      Author: Chiara Gastaldi
 */

# include <cassert>
#include <math.h>
#include <cmath>

#include "LUsolver.hpp"

// Constructor

LUsolver::LUsolver(DenseMatrix& A, VectorND& b, VectorND& x)
: LinearSystem(A,b,x)
{}
// Copy constructor

LUsolver::LUsolver(LUsolver& LU)
: LinearSystem(LU)
{}


// Destructor

LUsolver::~LUsolver()
{}


bool LUsolver::Singular()
{
	bool sin;
	double det=1;
	DenseMatrix L(mSize);
	L= ComputeL(mpA,mpb);
	for(int i=0; i<mSize;i++)
	{
		det=det*mpA.GetElement(i,i);

	}

	if(det!=0)
	{
		sin=false;
	}
	else
	{
		sin=true;
	}
	return sin;
}


//Gauss argorithm
DenseMatrix LUsolver::ComputeL(DenseMatrix& A, VectorND& b)
{
	DenseMatrix L(mSize);
	L.SetElement(0,0, 1);


	for (int k = 0; k < mSize-1; k++)
	{
		Pivot(b, k);
		for (int i = k+1 ; i < mSize; i++)//i=k+1
		{
			L.SetElement(i,k, A.GetElement(i,k) / A.GetElement(k,k));
			for (int j = k+1; j < mSize; ++j)
			{

				double f=A.GetElement(i,j);
				A.SetElement(i,j,f- A.GetElement(k,j)*L.GetElement(i,k));

			}

			L.SetElement(i,i, 1);
		}

	}
	return L;
}



void LUsolver::Pivot(VectorND& b, const int k)
{
	double max = std::fabs(mpA.GetElement(k,k));
	int idxmax = k;

	for (int i = k + 1; i < mSize; ++i)
	{
		double ev = std::fabs(mpA.GetElement(i,k));
		if (ev > max)
		{
			max = ev;
			idxmax = i;
		}
	}
}



VectorND LUsolver::Solve()
{
	DenseMatrix L(mSize);
	L= ComputeL(mpA,mpb);
	VectorND y(mSize);
	y = L.ForwardTriangularSolve(mpb);
	mpx = mpA.BackwardTriangularSolve(y);

	return mpx;
}







