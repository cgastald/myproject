/**
 * PCGsolver.hpp
 * Implementation of the preconditioned conjugate gradient method to solve linear systems
 *  Created on: Nov 27, 2014
 *      Author: Chiara Gastaldi
 */


/// definition of the class PCGsolver
#ifndef PCGSOLVER_HPP_
#define PCGSOLVER_HPP_

#include <iostream>

#include "DenseMatrix.hpp"
#include "VectorND.hpp"
#include "LinearSystem.hpp"
#include "AbstractPreconditioner.hpp"

class PCGsolver: public AbstractPreconditioner
{
  //public methods
public:
	/// Constructor
	/**
	 * The constructor takes a matrix
	 * and a vector and it is directly implemented from
	 * the constructor of the class "AbstractPreconditioner"
	 */
	PCGsolver(DenseMatrix& A, VectorND& b, VectorND& x);

	///copy constructor
	PCGsolver(PCGsolver& otherPCGsolver);


	/// Destructor
	~PCGsolver();

	/**
	 * ComputeD: computes the diagonal of the input matrix
	 * parameters: DenseMatrix& A
	 */
	DenseMatrix ComputeD(DenseMatrix& A);

	/**
	 * ComputeI: computes the identity matrix
	 * parameters: int size, the size of the identity matrix
	 */
	DenseMatrix ComputeI(int size);

	///Solve method
	/**
	 * Solve(void): solve the linear system using preconditioned conjugate gradient method
	 */
	VectorND Solve();

};



#endif /* PCGSOLVER_HPP_ */
