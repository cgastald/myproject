/*
 * AbstractPreconditioner.cpp
 *
 *  Created on: Nov 27, 2014
 *      Author: cgastald
 */

# include <cassert>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>

#include "AbstractPreconditioner.hpp"



AbstractPreconditioner::AbstractPreconditioner(DenseMatrix& A, VectorND& b, VectorND& x)
: LinearSystem(A,b,x),
  mP(A.size()), //contruction of the preconditioner
  mpAlpha(0)
{
  for(int i=0;i<A.size();i++)
  {
	  for(int j=0;j<A.size();j++)
	  {
		  mP(i,j)=0;
	  }
  }
}

AbstractPreconditioner::AbstractPreconditioner(AbstractPreconditioner& AB)
: LinearSystem(AB),
  mP(AB.GetP()),
  mpAlpha(0)
{}
// Destructor

AbstractPreconditioner::~AbstractPreconditioner()
{}


DenseMatrix AbstractPreconditioner::GetP()
{
	return mP;
}

void AbstractPreconditioner::SetP(DenseMatrix& A)
{
	mP=A;;
}

