/*
 * PG_test.cpp
 *
 *  Created on: Nov 27, 2014
 *      Author: cgastald
 */


#include <iostream>

#include "VectorND.hpp"
#include "AbstractMatrix.hpp"
#include "DenseMatrix.hpp"
#include "LinearSystem.hpp"
#include "AbstractPreconditioner.hpp"
#include "PGsolver.hpp"

int main(int argc, char* argv[])
{
	//--- FIRST TEST---

	// Declare two matrices
	DenseMatrix C1(3);
	C1(0,0)= 1.;
	C1(0,1)= 0.;
	C1(0,2)= 1.;
	C1(1,0)= 0.;
	C1(1,1)= 2.;
	C1(1,2)= 0.;
	C1(2,0)= 1.;
	C1(2,1)= 0.;
	C1(2,2)= 3.;
	DenseMatrix C2(3);
	C2(0,0)= 1.;
	C2(0,1)= 4.;
	C2(0,2)= 2.;
	C2(1,0)= 3.;
	C2(1,1)= 6.;
	C2(1,2)= 2.;
	C2(2,0)= 1.;
	C2(2,1)= 3.;
	C2(2,2)= 1.;

	VectorND b(3);
	b[1]=2.;
	b[2]=4.;
	b[0]=1.;
	VectorND c(3);
	VectorND x(3);

	PGsolver PG(C1,b,x);
	x=PG.Solve();
	std::cout << "the solution x is " <<	PG.Getx()  << std::endl;
	//the solution from C1 is x=(-0.5,1,1.5)

	//---SECOND TEST---

	DenseMatrix C3(3);
	C3(0,0)= 1.;
	C3(0,1)= -3.;
	C3(0,2)= 0.;
	C3(1,0)= -4.;
	C3(1,1)= 1.;
	C3(1,2)= 0.;
	C3(2,0)= 0.;
	C3(2,1)= 0.;
	C3(2,2)= 8.;

	VectorND f(3);
	f[1]=1.;
	f[2]=1.;
	f[0]=1.;
	VectorND d(3);
	VectorND y(3);
	DenseMatrix C4(3);

	PGsolver CH(C3,f,y);
	// example of wrong choice of the matrix-> error output
	y = CH.Solve();
	std::cout << "the solution y is " <<	y  << std::endl;


    return 0;
}

