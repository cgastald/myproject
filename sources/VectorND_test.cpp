/*
 * Vector_main.cpp
 *
 *  Created on: Oct 24, 2014
 *      Author: cgastald
 */

#include <iostream>

#include "VectorND.hpp"
#include "AbstractMatrix.hpp"
#include "DenseMatrix.hpp"

int main(int argc, char* argv[])
{
	// Declare two vectors
	double x[4]={1.0, 1.0, -1.0, 4.0};
	double y[4]={1.0, 1.0, -1.0, 4.0};

	VectorND v1 (4,x);
	VectorND v2 (4,y);
	v2.Print();
	std::cout << "\n" ;

	VectorND a(4);
	a = (v1 + v2);
	std::cout << "a = v1 + v2: " << a << std::endl;

	// b = v1 - v2
	VectorND b(4);
	b = v1 - v2;
	std::cout << "b = v1 - v2: " <<	b << std::endl;

	// c = z * v2
	VectorND c(4);
	double z=5.;
	c =  v2*z;
	std::cout << "c = v1 * z: " << c << std::endl;

	// d = v2 * v1
	double d;
	d= v2.dot( v1);
	std::cout << "d = v2 * v1: " << d << std::endl;


	std::cout << "c maxcomp: " << c.maxComp()<< std::endl;



    return 0;
}



