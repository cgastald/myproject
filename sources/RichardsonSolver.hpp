/**
 * RichardsonSolver.hpp
 * Implementation of the Richardson method to solve linear systems
 *  Created on: Dec 9, 2014
 *      Author: cgastald
 */

#ifndef RICHARDSONSOLVER_HPP_
#define RICHARDSONSOLVER_HPP_

#include <iostream>

#include "DenseMatrix.hpp"
#include "VectorND.hpp"
#include "LinearSystem.hpp"
#include "AbstractPreconditioner.hpp"

class RichardsonSolver: public AbstractPreconditioner
{
  //public methods
public:
	/// Constructor
	/**
	 * The constructor takes a matrix
	 * and a vector and it is directly implemented from
	 * the constructor of the class "AbstractPreconditioner"
	 */
	RichardsonSolver(DenseMatrix& A, VectorND& b, VectorND& x);

	///copy constructor
	RichardsonSolver(RichardsonSolver& RichardsonSolver);


	/// Destructor
	~RichardsonSolver();

	/**
	 * ComputeD: computes the diagonal of the input matrix
	 * parameters: DenseMatrix& A
	 */
	DenseMatrix ComputeD(DenseMatrix& A);

	/**
	 * ComputeI: computes the identity matrix
	 * parameters: int size, the size of the identity matrix
	 */
	DenseMatrix ComputeI(int size);


	///Solve method
	/**
	 * Solve(void): solve the linear system using preconditioned gradient method
	 */
	VectorND Solve();

private:




};




#endif /* RICHARDSONSOLVER_HPP_ */
