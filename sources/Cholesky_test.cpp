/*
 * Cholesky_test.cpp
 *
 *  Created on: Nov 24, 2014
 *      Author: cgastald
 */




#include <iostream>

#include "VectorND.hpp"
#include "AbstractMatrix.hpp"
#include "DenseMatrix.hpp"
#include "LinearSystem.hpp"
#include "CholeskySolver.hpp"

int main(int argc, char* argv[])
{
	// Declare two matrices
	DenseMatrix C1(3);
	C1(0,0)= 1.;
	C1(0,1)= 0.;
	C1(0,2)= 1.;
	C1(1,0)= 0.;
	C1(1,1)= 2.;
	C1(1,2)= 0.;
	C1(2,0)= 1.;
	C1(2,1)= 0.;
	C1(2,2)= 3.;

	VectorND b(3);
	b[1]=2.;
	b[2]=4.;
	b[0]=1.;
	VectorND c(3);
	VectorND x(3);
	DenseMatrix C2(3);

	CholeskySolver CHO(C1,b,x);
	C2=CHO.CholeskySolver::ComputeR(C1);
	std::cout << "R is " <<	C2 << std::endl;
	x = CHO.Solve();

	std::cout << "the solution x is " <<	x  << std::endl;

	// the solution is x=(-0.5,1,1.5)

	DenseMatrix C3(3);
	C3(0,0)= 1.;
	C3(0,1)= -4.;
	C3(0,2)= 0.;
	C3(1,0)= -4.;
	C3(1,1)= -1.;
	C3(1,2)= 0.;
	C3(2,0)= 0.;
	C3(2,1)= 0.;
	C3(2,2)= 8.;

	VectorND f(3);
	f[1]=1.;
	f[2]=1.;
	f[0]=1.;
	VectorND d(3);
	VectorND y(3);
	DenseMatrix C4(3);

	CholeskySolver CH(C3,f,y);
	// example of wrong choice of the matrix-> error output
	C4=CH.CholeskySolver::ComputeR(C3);
	std::cout << "R is " <<	C4 << std::endl;
	y = CH.Solve();

	std::cout << "the solution y is " <<	y  << std::endl;


    return 0;
}



