# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/cgastald/pcsc-exercises-2014/solutions/chapter-07/GraduateStudent.cpp" "/home/cgastald/pcsc-exercises-2014/build/opt/solutions/chapter-07/CMakeFiles/ch-07-ex-01-solution.dir/GraduateStudent.cpp.o"
  "/home/cgastald/pcsc-exercises-2014/solutions/chapter-07/PhDStudent.cpp" "/home/cgastald/pcsc-exercises-2014/build/opt/solutions/chapter-07/CMakeFiles/ch-07-ex-01-solution.dir/PhDStudent.cpp.o"
  "/home/cgastald/pcsc-exercises-2014/solutions/chapter-07/Student.cpp" "/home/cgastald/pcsc-exercises-2014/build/opt/solutions/chapter-07/CMakeFiles/ch-07-ex-01-solution.dir/Student.cpp.o"
  "/home/cgastald/pcsc-exercises-2014/solutions/chapter-07/ch-07-ex-01-solution.cpp" "/home/cgastald/pcsc-exercises-2014/build/opt/solutions/chapter-07/CMakeFiles/ch-07-ex-01-solution.dir/ch-07-ex-01-solution.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../../solutions"
  "../../solutions/chapter-07"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
