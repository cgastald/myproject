# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/cgastald/pcsc-exercises-2014/work/quiz02/Quaternion.cpp" "/home/cgastald/pcsc-exercises-2014/build/opt/work/quiz02/CMakeFiles/quiz-02-solution-quiz02.dir/Quaternion.cpp.o"
  "/home/cgastald/pcsc-exercises-2014/work/quiz02/Vector3D.cpp" "/home/cgastald/pcsc-exercises-2014/build/opt/work/quiz02/CMakeFiles/quiz-02-solution-quiz02.dir/Vector3D.cpp.o"
  "/home/cgastald/pcsc-exercises-2014/work/quiz02/quiz-02-solution.cpp" "/home/cgastald/pcsc-exercises-2014/build/opt/work/quiz02/CMakeFiles/quiz-02-solution-quiz02.dir/quiz-02-solution.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../../work"
  "../../work/quiz02"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
