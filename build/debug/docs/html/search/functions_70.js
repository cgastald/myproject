var searchData=
[
  ['pcgsolver',['PCGsolver',['../class_p_c_gsolver.html#aea0b605ffe63a579ad80f9f1a84ef11d',1,'PCGsolver::PCGsolver(DenseMatrix &amp;A, VectorND &amp;b, VectorND &amp;x)'],['../class_p_c_gsolver.html#a643005530ec25eab64b5dcc0b4617950',1,'PCGsolver::PCGsolver(PCGsolver &amp;otherPCGsolver)']]],
  ['pgsolver',['PGsolver',['../class_p_gsolver.html#a7d0698371fad6766bd2ea29032ce49ee',1,'PGsolver::PGsolver(DenseMatrix &amp;A, VectorND &amp;b, VectorND &amp;x)'],['../class_p_gsolver.html#a905a7aa9fe4ffa98682c9dba5fb9d17e',1,'PGsolver::PGsolver(PGsolver &amp;PGsolver)']]],
  ['print',['Print',['../class_abstract_matrix.html#a88a1770ae8d449ff574fe15b8ce38b94',1,'AbstractMatrix::Print()'],['../class_dense_matrix.html#a1bca40ba34558a75f92a5f23ef8a1f07',1,'DenseMatrix::Print()']]]
];
