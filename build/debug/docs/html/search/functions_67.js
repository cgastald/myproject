var searchData=
[
  ['gaussseidelsolver',['GaussSeidelSolver',['../class_gauss_seidel_solver.html#aef02f5c8fa895f709ded41959f530a5d',1,'GaussSeidelSolver::GaussSeidelSolver(DenseMatrix &amp;A, VectorND &amp;b, VectorND &amp;x)'],['../class_gauss_seidel_solver.html#a795acc4cdc5b4b712159979ae75cfafe',1,'GaussSeidelSolver::GaussSeidelSolver(GaussSeidelSolver &amp;otherGaussSeidelSolver)']]],
  ['geta',['GetA',['../class_linear_system.html#a1e29ceeb775667ee216673cba71f8efd',1,'LinearSystem']]],
  ['getb',['Getb',['../class_linear_system.html#a814d1a4b91b93085ea776971aab6018c',1,'LinearSystem']]],
  ['getelement',['GetElement',['../class_abstract_matrix.html#ae0856db8f2c764189b50ddce42ac56f0',1,'AbstractMatrix::GetElement()'],['../class_dense_matrix.html#ab2b8eb7a1fdaeabe834ab8ba0a10c566',1,'DenseMatrix::GetElement()'],['../class_vector_n_d.html#a6ec2059327d79b959c97275698eb853e',1,'VectorND::GetElement()']]],
  ['getm',['GetM',['../class_dense_matrix.html#ab6910f7e974ce376c5888ffd1a29d0b8',1,'DenseMatrix']]],
  ['getn',['GetN',['../class_vector_n_d.html#ab6ba030781c05f0ee0a8a04fb85a9bf6',1,'VectorND']]],
  ['getp',['GetP',['../class_abstract_preconditioner.html#aacfd88893ada4c2d2bbb574e701284fd',1,'AbstractPreconditioner']]],
  ['getsize',['GetSize',['../class_linear_system.html#af4307c4cdefa8dfeae21d95883846089',1,'LinearSystem']]],
  ['getv',['GetV',['../class_vector_n_d.html#a6d838b86403db6f201879077a91d325a',1,'VectorND']]],
  ['getx',['Getx',['../class_linear_system.html#a96a57e48a0c063b05259ec9595c90e85',1,'LinearSystem']]]
];
